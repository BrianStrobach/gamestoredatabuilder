//
//  Game.m
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import "Game.h"


@implementation Game

@dynamic appDescription;
@dynamic appName;
@dynamic artworkURL60;
@dynamic developerName;
@dynamic searchTerms;
@dynamic appStoreURL;

@end

//
//  BSViewController.m
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/19/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import "BSViewController.h"
#import "Networker.h"
#import "Game.h"
#import "StoreManager.h"

@interface BSViewController ()

@end

@implementation BSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSURL *itunesAPIRequstURL = [NSURL URLWithString:@"http://itunes.apple.com/search?term=family&media=software&entity=software,iPadSoftware&country=US&limit=250"];
    
    [Networker fetchContentsOfURL:itunesAPIRequstURL completion:^(NSData *data, NSError *error) {
        
        if (error == nil) {
            NSError *jsonParsingError = nil;
            NSDictionary *itunesJSON = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParsingError];
            NSManagedObjectContext *context = [StoreManager sharedStoreManager].managedObjectContext;
            
            if (jsonParsingError == nil) {
                NSMutableArray *games = [[NSMutableArray alloc]init];
                                NSLog(@"%@", itunesJSON);
                for (NSDictionary *game in [itunesJSON objectForKey:@"results"]) {
                    Game *newGame = [NSEntityDescription insertNewObjectForEntityForName:@"Game"
                                                                  inManagedObjectContext:context];
                    newGame.appName = [game objectForKey:@"trackName"];
                    newGame.appDescription = [game objectForKey:@"description"];
                    newGame.developerName = [game objectForKey:@"sellerName"];
                    newGame.artworkURL60 = [game objectForKey:@"artworkUrl60"];
                    newGame.appStoreURL = [game objectForKey:@"trackViewUrl"];
                    NSString *description = newGame.appDescription;
                    NSMutableString *searchTerms = [[NSMutableString alloc]init];
                    NSLinguisticTaggerOptions options = NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation;
                    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
                    tagger.string = description;
                    [tagger enumerateTagsInRange:NSMakeRange(0, [description length])
                                          scheme:NSLinguisticTagSchemeNameTypeOrLexicalClass
                                         options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
                                             NSString *token = [description substringWithRange:tokenRange];
                                             if ([tag isEqual:@"Noun"]) {
                                                 [searchTerms appendString:[NSString stringWithFormat:@" %@", token]];
                                             }
                                         }];
                    newGame.searchTerms = searchTerms;
                    
                    NSError *error;
                    if (![context save:&error]) {
                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    }
                    
                    
                    
                }
                NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Game"];
                NSArray *results = [context executeFetchRequest:request error:nil];
                CDLog(YES,@"Games:%@",results);
                
                
            }
            else NSLog(@"JSON parsing Error: %@", jsonParsingError);
        }
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

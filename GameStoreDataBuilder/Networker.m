//
//  Networker.m
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/19/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import "Networker.h"


@implementation Networker

+ (NSURLSession *)dataSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    });
    return session;
}

+ (void)fetchContentsOfURL:(NSURL *)url
                completion:(void (^)(NSData *data, NSError *error)) completionHandler {
    
    NSURLSessionDataTask *dataTask =
    [[self dataSession] dataTaskWithURL:url
                      completionHandler:
     
     ^(NSData *data, NSURLResponse *response, NSError *error) {
         
         if (completionHandler == nil) return;
         
         if (error) {
             completionHandler(nil, error);
             return;
         }
         completionHandler(data, nil);
     }];
    
    [dataTask resume];
}

+ (void)downloadFileAtURL:(NSURL *)url
               toLocation:(NSURL *)destinationURL
               completion:(void (^)(NSError *error)) completionHandler {
    
    NSURLSessionDownloadTask *fileDownloadTask =
    [[self dataSession] downloadTaskWithRequest:[NSURLRequest requestWithURL:url]
                              completionHandler:
     
     ^(NSURL *location, NSURLResponse *response, NSError *error) {
         
         if (completionHandler == nil) return;
         
         if (error) {
             completionHandler(error);
             return;
         }
         
         NSError *fileError = nil;
         [[NSFileManager defaultManager] removeItemAtURL:destinationURL error:NULL];
         [[NSFileManager defaultManager] moveItemAtURL:location toURL:destinationURL error:&fileError];
         completionHandler(fileError);
     }];
    
    [fileDownloadTask resume];
}

@end

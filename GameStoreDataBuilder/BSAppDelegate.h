//
//  BSAppDelegate.h
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/19/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

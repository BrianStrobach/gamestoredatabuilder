//
//  Game.h
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/22/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Game : NSManagedObject

@property (nonatomic, retain) NSString * appDescription;
@property (nonatomic, retain) NSString * appName;
@property (nonatomic, retain) NSString * artworkURL60;
@property (nonatomic, retain) NSString * developerName;
@property (nonatomic, retain) NSString * searchTerms;
@property (nonatomic, retain) NSString * appStoreURL;

@end

//
//  Networker.h
//  GameStoreDataBuilder
//
//  Created by Brian Strobach on 4/19/14.
//  Copyright (c) 2014 Hank5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Networker : NSObject


+ (void)fetchContentsOfURL:(NSURL *)url
                completion:(void (^)(NSData *data, NSError *error)) completionHandler;

+ (void)downloadFileAtURL:(NSURL *)url
               toLocation:(NSURL *)destinationURL
               completion:(void (^)(NSError *error)) completionHandler;
@end
